package com.spring.orm;

import java.io.InputStreamReader;
import java.io.BufferedReader;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.spring.orm.dao.StudentDao;
import com.spring.orm.entities.Student;

public class App {
	public static void main(String[] args) {
		// System.out.println("Hello World!");

		ApplicationContext context = new ClassPathXmlApplicationContext("config.xml");
		StudentDao studentDao = context.getBean("studentDao", StudentDao.class);
//		Student student = new Student(2325, "Ramesh", "Noida");
//		int r=studentDao.insert(student);
//		
//		System.out.println("done"+r);

		// CRUD Meaning
		// C-> Create
		// R-> Read
		// U-> Update
		// D-> Delete

		System.out.println("************* Welcome to Spring orm project *************");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		boolean go=true;
		while (go) {
			System.out.println("PRESS 1 for add new student");
			System.out.println("PRESS 2 for display all students");
			System.out.println("PRESS 3 for get detail of single student");
			System.out.println("PRESS 4 for delete student");
			System.out.println("PRESS 5 for update  student");
			System.out.println("PRESS 6 exit");
			try {
				int input = Integer.parseInt(br.readLine());
				switch (input) {
				case 1:
					
					break;
				case 2:
					break;
				case 3:
					break;
				case 4:
					break;
				case 5:
					break;
				case 6:
					go=false;
					break;
					

				default:
					break;
				}
			} catch (Exception e) {
				System.out.println("Invalid Input");
				System.out.println(e.getMessage());
			}
		}

	}
}
